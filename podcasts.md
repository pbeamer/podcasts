## Masters in Business

### Here are some of my favorite shows from the POD cast.
- Nick Murray https://ritholtz.com/2015/08/masters-in-business-nick-murray-financial-advisers-adviser/ Nick is an Advisor of Advisor's
- Emanuel Derman https://ritholtz.com/2016/02/mib-emanuel-derman/ Ran the Quantitative Strategies group at Goldman, Sachs. One of my favorite shows.
- Danny Kahneman https://ritholtz.com/2016/08/mib-danny-kahneman/ One of the godfathers of behavioral & cognitive psychology,  This is by far one of my favorite conversations
- Marc Andreessen https://ritholtz.com/2017/05/mib-marc-andreessen-venture-capitalist-a16z/
- Rich Barton https://ritholtz.com/2017/07/mib-rich-barton/
- John Carreyrou https://ritholtz.com/2018/07/mib-john-carreyrou-theranos-bad-blood/ He wrote the story that brought down Theranos.
- Rick Wilson https://ritholtz.com/2018/12/mib-rick-wilson/  Republican campaign consultant who just wrote the book "Everything Trump Touches Dies: A Republican Strategist Gets Real About the Worst President Ever"
- Ray Dalio https://ritholtz.com/2018/11/mib-ray-dalio-bridgewater-associates-2/
- Torsten Slok https://ritholtz.com/2018/08/mib-torsten-slok-deutsche/
- Yuval Noah Harari https://ritholtz.com/2017/03/mib-yuval-noah-harari/ Author of Sapiens: A Brief History of Humankind
